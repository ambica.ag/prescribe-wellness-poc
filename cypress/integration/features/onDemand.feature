Feature:Send an OnDemand Voice/SMS communication
    
    Scenario: Send an OnDemand via the 'Active Patients' patient list
        Given the pharmacist is logged into the PrescribeWellness website
        When the pharmacist navigates to "Active Patients" page
        And the pharmacist selects patient
            | Patient     |
            | Allen, Carl |
        And the pharmacist sends "OnDemand" campaign "COVID-19 Testing Recruitment_02"
        Then green toaster with message "Campaign has been started successfull!" is displayed
        When the pharmacist navigates to "Connections" page
        And the pharmacist is on "Completed" tab
        Then OnDemand communication record is displayed
            | Channel | Patient     | Campaign                        |
            | SMS     | Allen, Carl | COVID-19 Testing Recruitment 02 |

   


