///<reference types="Cypress" />
import { When } from "cypress-cucumber-preprocessor/steps";
import { dashboardPage } from "../../support/pages/dashboard";

When("the pharmacist navigates to {string} page", ((homeDropdown) => {
    dashboardPage.clickHomeDropdown(homeDropdown)
}))

When("the pharmacist navigates to {string}", ((dropdownValue) => {
    dashboardPage.clickGearIcon()
    dashboardPage.selectGearIconDropdownValue(dropdownValue)
}))

