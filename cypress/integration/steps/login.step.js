//<reference types="Cypress" />
import { Given } from "cypress-cucumber-preprocessor/steps";
import { loginPage } from "../../support/pages/login";

Given("the pharmacist is logged into the PrescribeWellness website", (() => {
  cy.clearCookies()
  cy.visit('/')
  loginPage.inputLoginValues('UserName', Cypress.env("username"))
  loginPage.inputLoginValues('Password', Cypress.env("password"))
  loginPage.clickLogin()
  loginPage.checkDashboardUrl()
}))
