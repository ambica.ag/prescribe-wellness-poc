///<reference types="Cypress" />
import { Then, When } from "cypress-cucumber-preprocessor/steps";
import { connectionsPage } from "../../support/pages/connections";
import {commonFunctions} from '../../support/common/commonFunctions'

When("the pharmacist is on {string} tab", ((connectionStatus) => {
    connectionsPage.clickTabsOnConnectionStatus(connectionStatus);
    commonFunctions.actionWait(5000)
}))

Then("OnDemand communication record is displayed", ((dataTable) => {
    dataTable.hashes().forEach(row => {
        connectionsPage.checkMessageStatus(row.Channel,row.Patient,row.Campaign)
    })
}))