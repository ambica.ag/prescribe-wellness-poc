///<reference types="Cypress" />
import { Then, When } from "cypress-cucumber-preprocessor/steps";
import { activePatientsPage } from "../../support/pages/activePatients";

When("the pharmacist selects patient",((dataTable) => {
    dataTable.hashes().forEach(row => {
        activePatientsPage.selectPatientCheckbox(row.Patient) 
    })
})) 

When("the pharmacist sends {string} campaign {string}", ((actionDropdownValue,campaign) => {
    activePatientsPage.navigateToCampaignModal(actionDropdownValue, campaign)
    activePatientsPage.clickBtnOnSendCommunication("Send Message")  
}))

Then("green toaster with message {string} is displayed", ((message) => {
    activePatientsPage.checkToastMessage(message)
    activePatientsPage.checkToasterColor('rgb(223, 240, 216)')
}))





