class CommonFunctions {

    //Method for fetching the Xpath/CSS locator JSON from fixtures
    getLocator(pageName) {
        var locators;
        switch (pageName) {
            case "loginPage":
                locators = require("../../fixtures/locators/login.json");
                break;
            case "dashboardPage":
                locators = require("../../fixtures/locators/dashboard.json");
                break;
            case "connectionsPage":
                locators = require("../../fixtures/locators/connections.json");
                break;
            case "activePatientsPage":
                locators = require("../../fixtures/locators/activePatients.json");
                break;
            default:
                locators = { "error": "Sorry! No PageName passed!" };
        }
        return locators;
    }

    //Formats the Xpath locator with the defined value array
    stringFormatter(locatorstring, argumentArray) {
        var reducer = function (locatorstring, argumentArray) {
            return locatorstring.replace(/%s/i, argumentArray);
        }
        //Reduce executes the 'reducer' function for each element in the 'argumentArray'
        return argumentArray.reduce(reducer, locatorstring);
    }
   
}
module.exports.commonFunctions = new CommonFunctions()



