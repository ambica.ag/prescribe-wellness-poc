import {commonFunctions} from '../common/commonFunctions'

class ConnectionsPage {

    constructor() {
        this.elementLocator = commonFunctions.getLocator('connectionsPage')
    }

    clickTabsOnConnectionStatus(connectionStatus) {
        cy.xpath(commonFunctions.stringFormatter(this.elementLocator.connectionStatus, [connectionStatus])).click()
    }

    //Method to verify onDemand communications sent to patient
    checkMessageStatus(channel, patient, campaign) {
        cy.xpath(commonFunctions.stringFormatter(this.elementLocator.onDemandVerification, [channel, patient, campaign]))
            .eq(0).then(($el) => {
                let connectDate = $el.text();
                cy.log("Connection sent date: " + connectDate)
                let currentDate = commonFunctions.getCurrentDate()
                expect(connectDate).to.equal(currentDate)
            })
    }
}
module.exports.connectionsPage = new ConnectionsPage()

