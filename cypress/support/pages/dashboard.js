import {commonFunctions} from '../common/commonFunctions'
class DashboardPage {

    constructor() {
        this.elementLocator = commonFunctions.getLocator('dashboardPage')
    }

    clickGearIcon() {
        cy.get(this.elementLocator.gearIcon).eq(1).click()
    }
   
    selectGearIconDropdownValue(dropdownValue) {
        cy.xpath(commonFunctions.stringFormatter(this.elementLocator.gearIconDropdownValues,[dropdownValue])).click({force: true})
    }

    //Select the options from the Home navigation
    clickHomeDropdown(homeDropdown) {
        cy.xpath(commonFunctions.stringFormatter(this.elementLocator.linksInHomeDropdown,[homeDropdown])).click()
    }
}
module.exports.dashboardPage = new DashboardPage()
