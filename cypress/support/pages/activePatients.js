import { commonFunctions } from '../common/commonFunctions'

class ActivePatientsPage {

    constructor() {
        this.elementLocator = commonFunctions.getLocator('activePatientsPage')
    }

    //Selects a patient/s 
    selectPatientCheckbox(patientName) {
        cy.xpath(commonFunctions.stringFormatter(this.elementLocator.patientCheckbox, [patientName])).click()
    }

    //Method for navigating to the onDemand campaign modal and selecting campaign
    navigateToCampaignModal(actionDropdownValue, campaign) {
        this.clickActionsDropdown(actionDropdownValue)
        this.clickCampaign(campaign)
        this.checkMessageIsNotEmpty()
    }

    clickActionsDropdown(actionDropdownValue) {
        cy.get(this.elementLocator.actionsDropdown).contains('Actions').click()
        cy.get(this.elementLocator.actionDropDownValues).contains(actionDropdownValue).click()
    }

    clickCampaign(campaign) {
        cy.xpath(commonFunctions.stringFormatter(this.elementLocator.campaignLink, [campaign])).click()
    }

    checkMessageIsNotEmpty() {
        cy.get(this.elementLocator.messageContent).then($el =>{
            expect($el).not.to.be.empty
        })
    }

    clickBtnOnSendCommunication(buttonName) {
        cy.get(this.elementLocator.sendCommunicationBtn).contains(buttonName).click()
    }

    //Verifying the toast message in the UI
    checkToastMessage(message) {
        cy.get(this.elementLocator.successMessageForOnDemand).then(($el) => {
            const toasterTest = $el.text()
            cy.log("Toaster text message: " + toasterTest)
            expect(toasterTest).to.equal(message)
        })
    }

    checkToasterColor(rgbColor) {
        cy.get(this.elementLocator.toasterColor).should('have.css', 'background-color', rgbColor)
    }
}
module.exports.activePatientsPage = new ActivePatientsPage()
