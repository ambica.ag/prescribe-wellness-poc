import {commonFunctions} from '../common/commonFunctions'

class LoginPage {

    constructor() {
        this.elementLocator = commonFunctions.getLocator('loginPage')
    }

    //Generic method for UserName and Password 
    inputLoginValues(fieldName,fieldValue) {
        return cy.xpath(commonFunctions.stringFormatter(this.elementLocator.loginValue,[fieldName])).type(fieldValue)
    }

    clickLogin() {
        return cy.get(this.elementLocator.submitbtn).click()
    }

    checkDashboardUrl() {
        cy.url().should('eq', 'https://beta-web.prescribewellness.com/Home/Dashboard');
    }
}
module.exports.loginPage = new LoginPage()